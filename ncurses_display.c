
//-----------------------------
// include standard

#include <ncurses.h>  // for printing it out
#include <stdlib.h>   //
//#include <iostream>
#include <uchar.h>  // for unicode
#include <argp.h>  // for parsing arguments
#include <stdio.h>
#include <string.h>  // written something that helps for now see below
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


//-----------------------------
// include variable library

#include "string_handling.h" // useful for strings and char[] and char*


//-----------------------------
// argument parser

// argument parser options
const char *argp_program_version = "ncurses_pid v0.1";
const char *argp_programm_bug_address = "<kuna42@web.de>";

/* Program documentation. */
static char doc[] = 
    "Ncurses_pid is a programm that allows to display interactive the output with the pid from another program."
    "\v"
    "Thanks for using this small programm. Please send ideas and improvements to <kuna42@web.de>.\n\n"
    "Author: Kuna42";
    

/* A description of the arguments we accept. */
static char args_doc[] = "ARGUMENT [STRING...]";

/* Keys for options without short-options. */
#define OPT_ABORT  1            /* –abort */

/* The options we understand. */
static struct argp_option options[] = {
  {"debug",     'd', 0,       0, "Debugging option" },
  {"input",     'i', "PID",   0, "pid of the process, that gives the input" },
  //TODO the input_pid have to be an integer
  // but now it will worked with a name

  { 0 }
};

/* Used by main to communicate with parse_opt. */
struct arguments_struct{
  char *argument;                /* argument */
  char **strings;                /* [string…] */
  bool debug;                    /* ‘-d’ */
  char *input_pid;                /* pid arg to ‘--input’ */
};

/* Parse a single option. */
static error_t
parse_opt (int key, char *arg, struct argp_state *state){
  /* Get the input argument from argp_parse, which we
     know is a pointer to our arguments structure. */
  struct arguments_struct *arguments = state->input;

  switch (key)
    {
    case 'd':
      arguments->debug = true;
      break;
    case 'i':
        //if (!isdigit(*arg)){
        //    return -1;
        //}
        arguments->input_pid = arg;
        break;

    case ARGP_KEY_NO_ARGS:
      argp_usage (state);

    case ARGP_KEY_ARG:
      /* Here we know that state->arg_num == 0, since we
         force argument parsing to end before any more arguments can
         get here. */
      arguments->argument = arg;

      /* Now we consume all the rest of the arguments.
         state->next is the index in state->argv of the
         next argument to be parsed, which is the first string
         we’re interested in, so we can just use
         &state->argv[state->next] as the value for
         arguments->strings.

         In addition, by setting state->next to the end
         of the arguments, we can force argp to stop parsing here and
         return. */
      arguments->strings = &state->argv[state->next];
      state->next = state->argc;

      break;

    default:
      return ARGP_ERR_UNKNOWN;
    }
  return 0;
}

/* Our argp parser. */
static struct argp argp = { options, parse_opt, args_doc, doc };




//-----------------------------
// ncurses

void quit_ncurses(){
    endwin();
} 

char* get_input(char* pid, int height_x_width){
    // get the input from a pipe
    int pipe;
    string input_string = create_string();
    string pipe_input_file = create_filled_string("/tmp/kuna42/");
    append_string(pipe_input_file, pid);
    
    if (access(pipe_input_file, F_OK) != 0){
        printf("Pipe doesn't exists\n");
        return create_string();//TODO how to better handle errors
    }
    // Open FIFO for Read only
    pipe = open(pipe_input_file, O_RDONLY);
    input_string = realloc(input_string, ((height_x_width * 12) + 1) * sizeof(char));
    read(pipe, input_string, height_x_width * 12);  // the *12 is for color support
    close(pipe);
    free(pipe_input_file);
    return input_string;
}

int show_output(struct arguments_struct arguments, char pipe_output_file[]){
//    get_input(arguments.input_pid);
    int own_pipe;
    string own_output = create_string();
    string input_string;
    //initscr();
    own_output = realloc(own_output, 10 + 1 + 10 + 1 + 2);
    //                         maximum  LINES, COLS = 10^10
    sprintf(own_output, "%d;%d#%c", LINES, COLS, ' ');
    
    // give the other program the information what size the window has
    own_pipe = open(&pipe_output_file[0], O_WRONLY);
    write(own_pipe, own_output, strlen(own_output)+1);
    close(own_pipe);
    
    while(true){
        input_string = get_input(&"ncurses_pid"[0], LINES*COLS);
        //curs_set(0);  // done by clear()
        clear();
        addstr(input_string);
        refresh();
    }
    free(own_output);
    free(input_string);
    clear();
    //endwin();
    return 0;
}

//-----------------------------
// main


int main (int argc, char **argv){
    int exit_code;
    int own_pipe;//
    struct arguments_struct arguments;
    string pipe_output_file = create_filled_string("/tmp/kuna42/");
    string own_pid = create_filled_string("ncurses_pid");//TODO this should be correct

    // Default values.
    arguments.debug = false;
    arguments.input_pid = 0;

    // make directory /tmp/kuna42/ if not exists
    struct stat st = {0};
    if (stat("/tmp/kuna42", &st) == -1){
        mkdir("/tmp/kuna42", 0666);
    }
    append_string(pipe_output_file, own_pid);
    mkfifo(pipe_output_file, 0666);

    /* Parse our arguments; every option seen by parse_opt will be
     reflected in arguments. */
    argp_parse (&argp, argc, argv, 0, 0, &arguments);

    
    // give the other program the information what size the window has
    own_pipe = open(pipe_output_file, O_WRONLY);
    
    write(own_pipe, "ncurses_pid", strlen("ncurses_pid")+1);
    close(own_pipe);
    
    initscr();
    atexit(quit_ncurses);  // what will be executed if it terminates
    exit_code = show_output(arguments, pipe_output_file);
    //endwin();
    remove(pipe_output_file);
    free(pipe_output_file);
    free(own_pid);
    exit(exit_code);
}