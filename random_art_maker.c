
//-----------------------------
// include standard
#include <stdlib.h>
#include <dirent.h>
#include <uchar.h>  // for unicode
#include <argp.h>   // for parsing arguments
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>


//-----------------------------
// include variable library

#include "string_handling.h"


//-----------------------------
// random generation - structs

struct ansi_image {
    char fields[][];//TODO
    int height;
    int width;
}

//-----------------------------
// random generation - methods

int max_height_and_width(char* display){
    //
    int height, width;
    int pipe;
    
    char input_string[25];
    char pipe_input_file = create_filled_string("/tmp/kuna42/");
    append_string(display
    strncat(display, "_display", 9);
        printf("Error in line above\n");
    strncat(pipe_input_file, display, 1000);//sizeof(pid));
    
    if (access(pipe_input_file, F_OK) != 0){
        return -1;//TODO how to better handle errors
    }
    pipe = open(pipe_input_file, O_RDONLY);
    read(pipe, input_string, (10+1+10+1+2));  // max size of window is 999_999_999 x 999_999_999
    close(pipe);
    bool semicolon_passed = false;
    for(int index; input_string[index] != 0; index++){
        if (input_string[index] == ';'){
            semicolon_passed = true;
            continue;
        }else if(input_string[index] == '#'){
            break;
        }
        if (!semicolon_passed){
            height = height * 10 + input_string[index] - '0';
        }else{
            width = width * 10 + input_string[index] - '0';
        }
    }
    return height, width;
}

char key_pressed(char display[20] = "ncurses"){
    int pipe;
    char input_string[25];
    char pipe_input_file[1000] = "/tmp/kuna42/";// + *pid;####################################### difference between char[] and char*
    strncat(display, "_display", 9);
    strncat(pipe_input_file, display, 1000);//sizeof(pid));
    
    if (access(pipe_input_file, F_OK) != 0){
        return -1;//TODO how to better handle errors
    }
    pipe = open(pipe_input_file, O_RDONLY);
    read(pipe, input_string, (10+1+10+1+2));  // max size of window and key pressed
    close(pipe);
    for(int index; input_string[index] != 0; index++){
        if ((input_string[index] == '#') && (input_string[index+1] != 0)){
            return input_string[index+1];
        }
    }
    return ' ';
}

int max_pictures_stock(char name[]){
    int file_counter = 0;
    char path[] = "";
    DIR * dir_p;
    struct dirent * entry;
    dir_p = opendir("path");
    while ((entry = readdir(dir_p)) != NULL){
        if (entry->d_type == DT_REG){
            file_counter++;
        }
    }
    closedir(dir_p);
    return file_counter;
}

int convert_picture_to_ansi(char picture_path[], int max_height, int max_width, char char_set[]){
    //
    char destination_path[] = "";
    char command[1000];
    sprintf(
        command, 
        "python3 jpg_to_ansi_art.py %s %s -size-columns %d -size-rows %d --ascii-chars %s", 
        picture_path,
        destination_path,
        max_height,
        max_width,
        char_set
    ); 
    system(command);
    return 0;
}

int create_pipe(){
}


int send_image_to_display(char image[]){
    int pipe;
    char pipe_input_file[1000] = "/tmp/kuna42/";// + *pid;####################################### difference between char[] and char*
    strncat(pipe_input_file, "pid-id", 6);
    pipe = open(pipe_input_file, O_WRONLY);
    write(pipe, image, strlen(image)+1);
    close(pipe);
    return 0;
}
//part of the real random...

//-----------------------------
// main

int main(int argc, char **argv){
    //
    max_height_and_width(&"ncurses"[0]);
    send_image_to_display("test");
    exit(0);
}