#!/bin/bash

CURRENT_PATH_TO_FOLDER = "$(dirname -- "$0")"

cd "$CURRENT_PATH_TO_FOLDER"

# setup venv python
source ".venv_linux/bin/activate"


# compile the program that display
g++ -x c ncurses_display.c -o ncurses_display -lncurses

# comile the program that makes random art
g++ -x c random_art_maker.c -o random_art_maker

# execute both at the same time (and set ncurses to the foreground)
"./random_art_maker" & ".ncurses_display" && fg

wait

#python3 jpg_to_ansi_art.py
deactivate