#!/usr/bin/env python3

import PIL.Image
# imports
import os
from argparse import ArgumentParser
# library
from PIL import Image


def open_image(path: str, new_width: int = 120, ascii_chars: tuple[str] = ("█", "▓", "▒", "░", " ")) -> str:
    #ascii_chars = ("█", "▓", "▒", "░", " ")  # ["#","%","=","+","~","-","\"","°","'","´"," "]

    image = Image.open(path)

    width, height = image.size
    aspect_ratio = height / width###
    new_height = int(aspect_ratio * new_width * 0.55)
    image = image.resize((new_width, new_height))
    #print(image.size)
    image = image.convert("L")

    pixel_str = ""
    pixels = image.getdata()
    for pixel in pixels:
        pixel_str += ascii_chars[(pixel * len (ascii_chars)) // 256]

    ascii_image = []
    for index in range(0, len(pixel_str), new_width):
        ascii_image.append(pixel_str[index:index+new_width])
    return "\n".join(ascii_image)


def save_image(path: str, ascii_image: str):
    with open(path, "w") as image_file:
        image_file.write(ascii_image + " ")


def convert_image(path: str, ascii_size_width: int):
    save_image(path.rsplit(".", 1)[0] + ".ansi-art",
               open_image(path, ascii_size_width))

def show_center(path: str) -> str:
    with open(path, "r") as image_file:
        ascii_image = image_file.readlines()
    for index, line in enumerate(ascii_image):
        ascii_image[index] = line[:-1]
    width, height = len(ascii_image[0]), len(ascii_image)
    middle_width = [width // 2]
    middle_height = [height // 2]
    if width % 2 - 1:
        middle_width.append(width // 2 - 1)
    if height % 2 - 1:
        middle_height.append(height // 2 - 1)
    #middle_width.reverse()
    print(f"{width}\n{middle_width}\n{height}\n{middle_height}")
    for middle_height_counter in middle_height:
        for middle_width_counter in middle_width:
            height_line = ascii_image[middle_height_counter]
            ascii_image[middle_height_counter] = (
                    height_line[:middle_width_counter] +
                    "\033[31;41m" +
                    height_line[middle_width_counter] +
                    "\033[m" +
                    height_line[middle_width_counter + 1:]
            )
    for index, line in enumerate(ascii_image):
        if index in middle_height:
            ascii_image[index] = "\033[31;41m║\033[m" + line + "\033[31;41m║\033[m"
        else:
            ascii_image[index] = "║" + line + "║"

    horizontal_boarder = "╔" + ("═" * width) + "╗"
    for middle_width_counter in middle_width:
        horizontal_boarder = (
                horizontal_boarder[:middle_width_counter + 1] +
                "\033[31;41m" +
                horizontal_boarder[middle_width_counter] +
                "\033[m" +
                horizontal_boarder[middle_width_counter + 2:]
        )
    ascii_image.insert(0, horizontal_boarder)
    horizontal_boarder = "╚" + horizontal_boarder[1:-1] + "╝"
    ascii_image.append(horizontal_boarder)
    ansi_art = "\n".join(ascii_image)
    return "\033[m" + ansi_art


if __name__ == "__main__":
    # Argument parser
    arg_parser = ArgumentParser(
        description="This is an image (jpg) to ansi-art converter",
        epilog="This programm use pillow.\n"
               "This is written by Kuna42.",
    )
    arg_parser.add_argument(
        "image",
        help="Path of the image to convert",
    )
    arg_parser.add_argument(
        "destination_folder",
        #"-d",
        help="Folder where the ansi-art will be saved",
    )
    arg_parser.add_argument(
        "--size-columns",
        "--height",
        help="Maximal height of the converted image, "
             "but the relation to the width is still the same.",
        type=int,
        default=100,
    )
    arg_parser.add_argument(
        "--size-rows",
        "--width",
        help="Maximal width of the converted image, "
             "but the relation to the height is still the same.",
        type=int,
        default=100,
    )
    arg_parser.add_argument(
        "--ascii-chars",
        "-c",
        help="A string with single chars that represents the depth of the color / greyness. "
             "the first char represents black, the last one white.\n"
             "Examples:\n"
             "BLOCK =   ' ░▒▓█'\n"
             "HIGH =    ' `-~+#@'\n"
             "LOW =     ' ¨'³•µðEÆ'\n"
             "OXXO =    '  .-=+*#%%@'\n"
             "DETAIL =  '#%%=+~-\"°'´ '",
        default="█▓▒░ ",
    )
    arguments = arg_parser.parse_args()
    save_image(
        path=arguments.destination_folder + arguments.image.rsplit("/", 1)[-1].rsplit(".", 1)[0] + ".ansi-art",
        ascii_image=open_image(
            path=arguments.image,
            new_width=arguments.size_rows,
            ascii_chars=tuple(arguments.ascii_chars),
        ),
    )
    #
    #image_path = input("image path: ")
    #convert_image(image_path, int(input("image size (as int): ")))
    #os.system("clear")
    #print(show_center(image_path.rsplit(".", 1)[0] + ".ansi-art"))