//-----------------------------
// useful for strings and char[] and char*

typedef char* string;

string create_string(){
    // create a allocated string
    return (string) calloc(2, sizeof(char));
}

string append_string(string char_pointer, char* string){
    // append the string to the char_pointer
    int char_pointer_size=0;
    while(char_pointer[char_pointer_size]){
        char_pointer_size++;
    }
    int string_size=0;
    while(string[string_size]){
        string_size++;
    }
    char_pointer = realloc(char_pointer, (char_pointer_size + string_size) * sizeof(char)); 
    // maybe there should be an *sizeof(char)?
    for (int index=0; index < string_size; index++){
        char_pointer[char_pointer_size + index] = string[index];
    }
    return char_pointer;
}

string create_filled_string(char* string){
    // define a string and fill it with another string
    return append_string(create_string(), string);
}

string set_string(string char_pointer, char* string){
    // clear the string first and then set it to the new value
    free(char_pointer);
    return create_filled_string(string);
}

//char* format_string(char* string, 
